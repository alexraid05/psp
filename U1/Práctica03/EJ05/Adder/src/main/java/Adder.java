import java.io.*;
import java.util.Scanner;

public class Adder {
    public static void main(String[] args) {

        //Declaramos las variables inputstreamreader y bufferedreader para obtener los parametros de entrada.

        String entrada = args[0];

        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try{

            //Abrimos el fichero y creamos un BufferedReader para poder
            //hacer una lectura de este con el método readLine().
            archivo = new File(entrada);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            //Lectura del fichero
            String linea;
            int suma=0;

            //Devolvemos las lineas del fichero
            while ((linea=br.readLine())!=null){
                suma = suma + Integer.parseInt(linea);
            }
            System.out.println(suma);

        }catch (IOException e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }finally {
            //Cerraremos el fichero para asegurarnos de que se cierra independientemente
            //de que haya ido todo bien o haya saltado algun error.
            try{
                if(null != fr){
                    fr.close();
                }
            }catch (Exception e2){
                e2.printStackTrace();
            }
        }
    }
}
