import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Traductor_Hijo {
    public static void main(String[] args) {

        //Declaramos nuestras variables para obtener los parametros de entrada
        InputStreamReader stream = new InputStreamReader(System.in);
        BufferedReader bffReader = new BufferedReader(stream);

        //Creamos el diccionario interno de nuestro programa Hijo.
        Map<String, String> diccionario = new HashMap<String, String>();
        diccionario.put("hola", "hi");
        diccionario.put("adios", "bye");
        diccionario.put("bien", "good");
        diccionario.put("mal", "bad");

        //En primer lugar traduciremos las palabras introducidas por el usuario mediante un diccionario interno.
        try {

            String frase = "";

            do {
                //Leemos la linea que entra como parametro cada vez hasta que se introduzca "finalizar".
                frase = bffReader.readLine();
                //Separamos las palabras de la linea de entrada
                String[] palaAPalabra = frase.split(" ");

                String resultado = "";
                for (int i = 0; i < palaAPalabra.length; i++) {
                    resultado = resultado + diccionario.get(palaAPalabra[i]) + " ";
                }
                System.out.println(resultado);

            } while (!frase.equals("finalizar"));

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}