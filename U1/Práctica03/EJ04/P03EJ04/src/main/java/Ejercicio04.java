import java.io.*;
import java.util.Scanner;

public class Ejercicio04 {
    private final static String finalizar = "finalizar";

    public static void main(String[] args) {

        String command="java -jar P03EJ04_Padre.jar";

        ProcessBuilder proccesTrans = new ProcessBuilder(command.split(" "));

        try {
            Process transf = proccesTrans.start();

            OutputStream os = transf.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner transfSc = new Scanner(transf.getInputStream());
            Scanner sc = new Scanner(System.in);

            String line = sc.nextLine();

            String resultado = "";
            String writeAll = "";

            do {
                bw.write(line);
                bw.newLine();
                bw.flush();

                resultado = transfSc.nextLine();

                writeAll = writeAll + line + " ->> " + resultado + "\n";

                line = sc.nextLine();
            } while (!line.equals(finalizar));

            FileWriter fichero = null;
            PrintWriter pw = null;

            try {

                fichero = new FileWriter("Translations.txt");
                pw = new PrintWriter(fichero);
                pw.println("<original_word> ->> <translation>");

                pw.println(writeAll);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // Nuevamente aprovechamos el finally para
                    // asegurarnos que se cierra el fichero.
                    if (null != fichero)
                        fichero.close();
                } catch (Exception e2) {
                    e2.printStackTrace();

                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
