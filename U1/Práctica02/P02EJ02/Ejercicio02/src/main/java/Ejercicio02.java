import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejercicio02 {

    public static void main(String args[])
    {

        String command = "java -jar Random10.jar";

        /*
          ProcessBuilder por defecto Hace que System.in y
          System.out estén conectadas al proceso padre
          y conecta al padre y el hijo a través de truberias
          con las que trabajaremos con getOutputStream y
          getInputStream
         */

        ProcessBuilder pb = new ProcessBuilder(command.split(" "));

        try {
            Process random10 = pb.start();

            /*

              Primero obtengo el flujo de escritura que
              me permite escribir en la entrada estándar
              del proceso hijo.

             */
            OutputStream os = random10.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            /*
                Ahora obtengo el flujo de salida del proceso
                hijo para leerla desde el padre con random10Sc

             */
            Scanner random10Sc = new Scanner(random10.getInputStream());

            /*
            Necesitamos leer la entrada del proceso Padre, esto lo
            realizamos mediante Scanner de System.in
            */

            Scanner sc = new Scanner(System.in);

            /*
            Vamos a crear una variable que almacene la frase
            leída desde la entrada estándar
            */

            String linea = sc.nextLine();

            while( ! linea.equals("stop")){

                bw.write(linea); // Damos la orden de escritura en el buffer
                bw.newLine(); // Enviamos el salto de línea
                bw.flush(); // Obligamos a que efectivamente se escriba en el Buffer
                System.out.println(random10Sc.nextLine()); //Leemos y mostramos lo que ha escrito el proceso por pantalla
                linea = sc.nextLine(); //Leemos lo que el usuario está introduciendo

            }


        } catch (IOException ex) {
            Logger.getLogger(Ejercicio02.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}