import java.io.*;

import org.w3c.dom.ls.LSOutput;

import java.util.Random;
import java.util.Scanner;

public class Random10 {

    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);

        Random rand = new Random();

        int lowerbound=0;
        int upperbound = 10;

        while (!sc.nextLine().equals("stop")){

            System.out.println(rand.nextInt(upperbound-lowerbound) + lowerbound);

        }
    }

}
