# PSP

2DAM. Programación de servicios y procesos.

Alejandro Romero Talledo

## Ejercicios U1

Práctica 02

    Ejercicio 01: https://gitlab.com/alexraid05/psp/-/tree/main/U1/Pr%C3%A1ctica02/P02Ej01

    Ejercicio 02: https://gitlab.com/alexraid05/psp/-/tree/main/U1/Pr%C3%A1ctica02/P02EJ02
    
    Ejercicio 03: https://gitlab.com/alexraid05/psp/-/tree/main/U1/Pr%C3%A1ctica02/P02EJ03
    
Práctica 03:

    Ejercicio 04: https://gitlab.com/alexraid05/psp/-/tree/main/U1/Pr%C3%A1ctica03/EJ04
    Incompleto.- Falta declarar entrada como File y Diccionario.

    Ejercicio 05: https://gitlab.com/alexraid05/psp/-/tree/main/U1/Pr%C3%A1ctica03/EJ05
    Incompleto.- No se ha podido obtener la suma de varios procesos.


